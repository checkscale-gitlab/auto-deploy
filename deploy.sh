#/bin/sh -e

[[ "$TRACE" ]] && set -x

export TILLER_NAMESPACE=$KUBE_NAMESPACE
export HELM_HOST="localhost:44134"

function docker_build() {
  docker build -t "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}" -f "${DOCKERFILE:-Dockerfile}" .
  docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}"
}

function docker_delete() {
  reg rm -d -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}"
}

function ensure_namespace() {
  echo "check if namespce $KUBE_NAMESPACE exists..."
  kubectl get namespace "$KUBE_NAMESPACE" || kubectl create namespace "$KUBE_NAMESPACE"
}

function initialize_tiller() {
  ensure_namespace

  echo "initializing helm..."
  helm init --client-only

  nohup tiller -listen ${HELM_HOST} -alsologtostderr >/dev/null 2>&1 &
  echo "Tiller is listening on ${HELM_HOST}"

  if ! helm version --debug; then
    echo "Failed to init Tiller."
    return 1
  fi
  echo ""
}

function helm_deploy() {
  docker_build
  initialize_tiller
  helm_download_chart

  if [ -f "values.yaml" ]; then
    cp values.yaml helm/
  fi

  sed -i -e "s/CI_COMMIT_SHORT_SHA/${CI_COMMIT_SHORT_SHA}/g" helm/Chart.yaml
  sed -i -e "s/CI_COMMIT_REF_SLUG/${CI_COMMIT_REF_SLUG}/g" helm/Chart.yaml

  # if [ "${CI_PROJECT_PATH_SLUG}" -eq "sii-tday-devops-ms-k8s-frontend" ]; then
  #   if [ "$CI_ENVIRONMENT_NAME" -eq "staging" ]; then
  #     echo "In frontend staging"
  #     KUBE_INGRESS_PREFIX_DOMAIN="staging."
  #   fi
  # fi

  helm upgrade --install \
    --force \
    --wait \
    --namespace "${KUBE_NAMESPACE}" \
    --version "${CI_COMMIT_SHORT_SHA}" \
    --set releaseOverride="${CI_ENVIRONMENT_SLUG}" \
    --set image.repository="${CI_REGISTRY_IMAGE}" \
    --set image.tag="${CI_COMMIT_REF_SLUG}" \
    --set gitlab.env="${CI_ENVIRONMENT_SLUG}" \
    --set gitlab.app="${CI_PROJECT_PATH_SLUG}" \
    --set gitlab.envName="${CI_ENVIRONMENT_NAME}" \
    --set gitlab.envURL="${CI_ENVIRONMENT_URL}" \
    --set ingress.domain="${KUBE_INGRESS_PREFIX_DOMAIN}${CI_PROJECT_PATH_SLUG}.${KUBE_INGRESS_BASE_DOMAIN}" \
    --set ingress.topdomain.enabled="${KUBE_INGRESS_TOP_ENABLED}" \
    --set ingress.topdomain.domain="${KUBE_INGRESS_PREFIX_DOMAIN}${KUBE_INGRESS_BASE_DOMAIN}" \
    --set ingress.topdomain.path="${KUBE_INGRESS_TOP_PATH}" \
    --set postgresql.enabled="${POSTGRES_ENABLED:-true}" \
    --set service.containerPort="${SERVICE_CONTAINERPORT:-8080}" \
    --set service.livenessProbe="${SERVICE_LIVENESSPROBE:-/health/live}" \
    --set service.readinessProbe="${SERVICE_READINESSPROBE:-/health/ready}" \
    --set quarkus.enabled="${QUARKUS_ENABLED:-true}" \
    "${CI_ENVIRONMENT_SLUG}" \
    helm
}

function helm_delete() {
  initialize_tiller
  helm delete --purge ${CI_ENVIRONMENT_SLUG}
}

function helm_download_chart() {
  helm_default_repo=sii-tday-devops/ms-k8s/helm-quarkus
  git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@${GITLAB_URL:-gitlab.com}/${HELM_REPO:-$helm_default_repo}.git helm
  helm dependency build helm/
}

##
## End Helper functions

option=$1
case $option in

  ensure_namespace) ensure_namespace ;;
  helm_deploy) helm_deploy ;;
  helm_delete) helm_delete ;;
  docker_build) docker_build ;;
  docker_delete) docker_delete ;;
  *) exit 1 ;;
esac
