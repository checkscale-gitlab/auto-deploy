FROM docker:latest

ARG REG_VERSION="v0.16.1"
ARG HELM_VERSION="v2.16.1"

COPY deploy.sh /usr/bin/auto-deploy

RUN apk add -U \
        openssl \
        curl \
        tar \
        gzip \
        bash \
        ca-certificates \
        git \
    ## Install Kubectl
    && KUBERNETES_VERSION=$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt) \
    && curl -L -o /usr/bin/kubectl "https://storage.googleapis.com/kubernetes-release/release/${KUBERNETES_VERSION}/bin/linux/amd64/kubectl" \
    && chmod +x /usr/bin/kubectl \
    && kubectl version --client \
    # Install Helm
    && curl "https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz" | tar zx \
    && mv linux-amd64/helm /usr/bin/ \
    && mv linux-amd64/tiller /usr/bin/ \
    && rm -rf linux-amd64 \
    # Install Reg
    && curl -fSL "https://github.com/genuinetools/reg/releases/download/${REG_VERSION}/reg-linux-amd64" -o "/usr/bin/reg" \
    && chmod +x "/usr/bin/reg" \
    # Auto deploy script
    && chmod +x /usr/bin/auto-deploy
